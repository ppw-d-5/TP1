from django.apps import AppConfig


class AppRafikaConfig(AppConfig):
    name = 'app_rafika'
