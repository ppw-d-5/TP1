from django.db import models

# Create your models here.

class Program(models.Model):
    judul = models.CharField(max_length=100, primary_key=True)
    isi = models.TextField(default='test cobain')
    foto = models.CharField(max_length = 350, default='#')