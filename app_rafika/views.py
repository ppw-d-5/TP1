from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Program


def beranda(request):
	return render(request, 'new_landingpage.html')

def thankyouPage(request):
	return render(request, 'thankyouPage.html')

def program_view(request, judul):
    program = get_object_or_404(Program, pk=judul)
    return render(request, 'new_landingpage.html', {'judul': program.judul,'isi':program.isi,'foto': program.gambar})



