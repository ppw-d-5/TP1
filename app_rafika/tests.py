from django.test import TestCase
from django.test import Client
from django.urls import resolve

class Unit_Test_TP1(TestCase):
	def test_beranda_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
		text_response = response.content.decode('utf8')
		self.assertIn("Program Kami", text_response)

	# def test_programKami_url_is_exist(self):
	# 	response = Client().get('/programKami/')
	# 	self.assertEqual(response.status_code, 200)
	# 	text_response = response.content.decode('utf8')
	# 	self.assertIn("Program Kami", text_response)

	def test_thankyouPage_url_is_exist(self):
		response = Client().get('/thankyouPage/')
		self.assertEqual(response.status_code, 200)
		text_response = response.content.decode('utf8')
		self.assertIn("Terima Kasih atas Donasi Anda!", text_response)

	

	

	

