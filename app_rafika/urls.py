from django.conf.urls import url
from .views import thankyouPage, beranda, program_view

urlpatterns = [
    url(r'^$', beranda, name ='beranda'),
    # url(r'^programKami/', programKami, name ='programKami'),
    url(r'^thankyouPage/', thankyouPage, name ='thankyouPage'),
    url(r'^<str:judul>', program_view)
    
    ]
