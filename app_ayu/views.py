from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from .models import news, Testimoni

response = {}

def tentangkami(request):
    return render(request,'tentangkami.html')

def news_func(request):
    all_news = news.objects.all()
    return render(request,'news.html', {'all_news': all_news})

# def komentar(request):
# 		if request.method == 'POST':
# 			text = request.POST['text']
# 			name = request.session['first_name']
# 			Testimoni.objects.create(username=name, text=text)
# 			return JsonResponse({'saved':True})
# 		return JsonResponse({'saved':False})

def jsonComment(request):
	
	usernama = request.user.first_name + " " + request.user.last_name
	testimoni = request.POST['comment']
	testimoni_model = Testimoni(username=usernama , comment = testimoni)
	
	testimoni_model.save()
	time = testimoni_model.time.strftime("%b. %d, %Y, %I:%M %p")
	return JsonResponse({'username': usernama, 'testimoni': testimoni, 'time': time})

def allComment(request):
	allComment = Testimoni.objects.all().values('username','comment','time')
	allComment2 = list(allComment)
	return JsonResponse(allComment2, safe=False)





