from django.db import models
from datetime import datetime,date
from django.utils import timezone

# Create your models here.
class news(models.Model):
    judul = models.CharField(max_length = 100)
    penggalan = models.CharField(max_length = 550)
    gambar = models.CharField(max_length = 100)
    lanjut = models.CharField(max_length = 100)

class Testimoni(models.Model):
	username = models.CharField(max_length=100)
	comment = models.CharField(max_length=500, null=True)
	time = models.DateTimeField(auto_now_add = True, null = True, blank = True)

	# def __str__(self):
	# 	return self.name