from django.test import TestCase
from django.test import Client
from django.urls import resolve

class Ayu_Test(TestCase):
    def test_tentangkami_url_is_exist(self):
        response = Client().get('/app_ayu/tentangkami/')
        self.assertEqual(response.status_code, 200)
        text_response = response.content.decode('utf8')
        self.assertIn("Founder", text_response)

    def test_news_url_is_exist(self):
        response = Client().get('/app_ayu/news/')
        self.assertEqual(response.status_code, 200)
        text_response = response.content.decode('utf8')
        self.assertIn("BERITA TERKINI", text_response)