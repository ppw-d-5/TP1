from django.db import models

class Donasi(models.Model):
	jumlah_donasi = models.IntegerField()
	first_name = models.CharField(max_length=100)
	email = models.EmailField(max_length=100, default='you default value or example@email.com')
	transferNamaBank = models.CharField(max_length=20)
	transferUntukProgram = models.CharField(max_length=200, default="Donasi ikhlas untuk semua program")
	is_anon = models.BooleanField(default=False)

	def __str__(self):
		return self.transferUntukProgram
# Create your models here.
