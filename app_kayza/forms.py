from django import forms
from .models import *

# input your forms here
class Donasi_Form(forms.Form):
	OPTION_NAMABANK = (
		('BNI', 'BNI'),
		('BCA', 'BCA'),
	)

	OPTION_PROGRAM = (
		('Donggala Butuh Kita', 'Donggala Butuh Kita'),
		('Sisihkan Makananmu Untuk Papua', 'Sisihkan Makananmu Untuk Papua'),
		('Indonesia Bersama Lombok', 'Indonesia Bersama Lombok'),
	)
	jumlah_donasi = forms.IntegerField(label="Jumlah Donasi", required=True, min_value=1)
	transferNamaBank = forms.ChoiceField(label="Transfer dari bank(BNI/BCA)", required=True, choices=OPTION_NAMABANK)
	transferUntukProgram = forms.ChoiceField(label="Transfer untuk Program", required=True, choices=OPTION_PROGRAM)
	is_anon = forms.BooleanField(label="Donasi sebagai anonim",initial=False, required=False)