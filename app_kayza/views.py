from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
from .forms import *
from app_aulia.models import Signup
from app_aulia.forms import Form_Signup


response = {}
# Create your views here.

def donasi(request):
	form = Donasi_Form(request.POST or None)
	if(request.method == "POST"):
		form = Donasi_Form(request.POST or None)
		if(form.is_valid()):
			if request.POST.get('is_anon', False):
				response['first_name'] = 'Anonim'
				request.session['email'] = request.user.email
				response['jumlah_donasi'] = form.cleaned_data['jumlah_donasi']
				response['transferNamaBank'] = form.cleaned_data['transferNamaBank']
				response['transferUntukProgram'] = form.cleaned_data['transferUntukProgram']
				Donasi.objects.create(first_name = response['first_name'], email=response['email'], jumlah_donasi=response['jumlah_donasi'], transferNamaBank=response['transferNamaBank'], transferUntukProgram=response['transferUntukProgram'])
			else:
				request.session['first_name'] = request.user.first_name
				request.session['email'] = request.user.email
				response['first_name'] = request.session['first_name']
				print(response['first_name'])
				response['email'] = request.session['email']
				print(response['email'])
				response['jumlah_donasi'] = form.cleaned_data['jumlah_donasi']
				response['transferNamaBank'] = form.cleaned_data['transferNamaBank']
				response['transferUntukProgram'] = form.cleaned_data['transferUntukProgram']
				Donasi.objects.create(first_name = response['first_name'], email=response['email'], jumlah_donasi=response['jumlah_donasi'], transferNamaBank=response['transferNamaBank'], transferUntukProgram=response['transferUntukProgram'])
			return redirect('/history')
		else:
			return render(request, 'formDonasi.html', response)
	else:
		response['form'] = form
		return render(request, 'formDonasi.html', response)

# def donatur(request):
# 	donatur = Donasi.objects.all()
# 	response['donatur'] = donatur
# 	response['Donasi_Form'] = Donasi_Form
# 	return render(request, 'formDonasi.html', response)

			# if(is_anon == True):
			# 	Donasi.objects.create(
			# 		email_donasi = email_donasi,
			# 		jumlah_donasi = jumlah_donasi,
			# 		transferNamaBank = transferNamaBank,
			# 		transferAtasNama = "Anonim",
			# 		transferUntukProgram = transferUntukProgram,
			# 		is_anon = True,
			# 	)
			# elif(is_anon == False):
			# 	Donasi.objects.create(
			# 		email_donasi = email_donasi,
			# 		jumlah_donasi = jumlah_donasi,
			# 		transferNamaBank = transferNamaBank,
			# 		transferAtasNama = transferAtasNama,
			# 		transferUntukProgram = transferUntukProgram,
			# 		is_anon = False,
			# 	)
