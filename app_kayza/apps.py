from django.apps import AppConfig


class AppKayzaConfig(AppConfig):
    name = 'app_kayza'
