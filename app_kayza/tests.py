from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import *
from .forms import *
from app_aulia.models import Signup
from app_aulia.forms import Form_Signup

# Create your tests here.
class TP1UnitTest(TestCase):
	# ngecek url ada apa engga 
	def test_tp1_url_is_exist(self):
		response = Client().get('/donasi')
		self.assertEqual(response.status_code, 200)

	def test_tp1_has_welcome_text(self):
		response = Client().get('/donasi')
		text = response.content.decode('utf8')
		self.assertIn("Berbagi senyuman, ringankan beban, <br>dan ulurkan tangan mu untuk mereka!", text)

	# menggunakan fungsi status
	def test_tp1_use_donasi(self):
		found = resolve('/donasi')
		self.assertEqual(found.func, donasi)

	# bisa menambah status baru	
	def test_tp1_can_create_jumlah_donasi(self):
		# creating a new status
		new_donasi = Donasi.objects.create(jumlah_donasi = 500000, transferNamaBank = "BNI", transferUntukProgram = "Donggala Butuh Kita", is_anon = False)

		# retrieving
		count_all_available_donasi = Donasi.objects.all().count()
		self.assertEqual(count_all_available_donasi, 1)

	# kalo blank formnya
	def test_form_validation_for_blank_items(self):
		form = Donasi_Form(data={'jumlah_donasi': '', 'transferNamaBank': '','transferUntukProgram': '', 'is_anon':''})
		self.assertFalse(form.is_valid())

	# # test apakah sukses buat ngerender
	# def test_tp1_post_success_and_render_the_result(self):
	# 	test = 'Berbagi senyuman, ringankan beban, <br>dan ulurkan tangan mu untuk mereka!'
	# 	response_post = Client().post('/donasi', {'first_name':'Kayza', 'email': 'akayza@mail.com', 'jumlah_donasi': 500, 'transferNamaBank': "BNI",'transferUntukProgram': "Donggala Butuh Kita", 'is_anon':False})
	# 	self.assertEqual(response_post.status_code, 200)

	# 	response = Client().get('/donasi')
	# 	html_response = response.content.decode('utf8')
	# 	self.assertIn(test, html_response)

	# kalo error yy
	def test_tp1_post_error_and_render_the_result(self):
		test = 'Berbagi senyuman, ringankan beban, <br>dan ulurkan tangan mu untuk mereka!'
		response = Client().post('/donasi',{})
		self.assertEqual(response.status_code, 200)

		response = Client().get('/donasi')
		html_response = response.content.decode('utf8')
		self.assertNotIn('Rafika', html_response)

	def test_if_anon_true(self):
		new_donasi = Donasi.objects.create(jumlah_donasi = 500000, transferNamaBank = "BNI", transferUntukProgram = "Donggala Butuh Kita", is_anon = True)
		test = Donasi.objects.all()
		self.assertNotIn("Kayza", test)

	def test_if_anon_false(self):
		new_donasi = Donasi.objects.create(jumlah_donasi = 500000, transferNamaBank = "BNI", transferUntukProgram = "Donggala Butuh Kita", is_anon = False)
		test = Donasi.objects.all()
		self.assertNotIn("Kayza", test)
