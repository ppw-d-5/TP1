from django.urls import path
from django.contrib import admin
from . import views 
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import *

urlpatterns = [
    path('', views.donasi, name="donasi"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
