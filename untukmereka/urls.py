from django.urls import path
from django.contrib import admin
from . import views 
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', views.homepage, name="homepage"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)