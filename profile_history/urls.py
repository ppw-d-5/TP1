from django.urls import path
from django.contrib import admin
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import *

urlpatterns = [
    path('', donaturList, name="history"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)