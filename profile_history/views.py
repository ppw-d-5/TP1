from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate
from django.http import JsonResponse
from app_kayza.models import Donasi
from app_kayza.forms import Donasi_Form
from django.contrib.auth.decorators import login_required

response = {}

# def listDonasiUser(request):
# 	if request.user.is_authenticated:
# 		if request.user.is_active:
# 			donatur = Donasi.objects.all().filter(email=request.user.email)
# 			response['donatur'] = donatur
# 			response['Donasi_Form'] = Donasi_Form
# 			return render(request, 'profile.html', {})
# 		else:
# 			return render(request, 'LoginGoogle.html', {})
# 	else:
# 		return render(request, 'LoginGoogle.html', {})

# def listDonasiUser(request):
# 	if request.user.is_authenticated:
# 		donatur = Donasi.objects.all().filter(email=request.user.email)
# 		response['daftar_donasi_donatur'] = donatur
# 		return render(request, 'profile.html', response)
# 	else:
# 		return render(request, 'LoginGoogle.html')

# Create your views here.

@login_required
def donaturList(request):
	email = request.session['email']
	name = request.session['name']
	if request.method == "POST":
		postEmail = request.POST['email']
		listProgram = Donasi.objects.all().filter(email=postEmail)
		total = 0
		list = []
		for data in listProgram:
			total += data.jumlah_donasi
			temp = {"program":data.transferUntukProgram, 'jumlahDonasi': data.jumlah_donasi}
			list.append(temp)
		return JsonResponse({'nama':name, 'list':list, 'total':total})
	return render(request, 'profile.html', {'name': name, 'email': email})
