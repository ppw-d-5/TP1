var listDonatur = function(email){
	var csrftoken = $("[name=csrfmiddlewaretoken]").val();
	$.ajax({
		method: "POST",
		url: "/history",
		headers:{"X-CSRFToken": csrftoken},
		data: {email: email},
		success: function(result){
			console.log(result);
			var html = "<h4 class=\"modal-title\">" + "Jumlah Donasi: " + result.total + "</h4>";
			$('.modal-title').replaceWith(html);
			var list = result.list;
			var start = $('.start');
			start.empty();
			for(var x = 0; x < list.length; x++){
				start.append("<div class=\"table\">" + list[x].program + ": Rp" + list[x].jumlahDonasi + "</div>")
			}
		},
		error: function(error){
			alert("Ada kesalahan teknis, mohon coba lagi")
		}
	})
};