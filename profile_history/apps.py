from django.apps import AppConfig


class ProfileHistoryConfig(AppConfig):
    name = 'profile_history'
