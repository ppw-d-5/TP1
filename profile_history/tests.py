from django.test import TestCase
from django.test import Client
from django.urls import resolve
from importlib import import_module
from django.contrib.auth.models import User
from django.conf import settings
from django.contrib.auth import authenticate

# Create your tests here.
class HistoryTP2UnitTest(TestCase):
	def test_history_not_logged_in(self):
		response = Client().get('/history')
		self.assertEqual(response.status_code, 302)

	def test_profile_logged_in(self):
		self.user = User.objects.create(username='kayza', password='12345')
		self.user.set_password('yooo')
		self.user.save()
		self.client.login(username='kayza', password='yooo')
		a = self.client.session
		a['email'] = 'kayza@mail.com'
		a['name'] = 'cuy'
		a.save()
		response = self.client.get('/history')
		self.assertEqual(response.status_code, 200)

# class SessionTestCase(TestCase):
#     def setUp(self):
#         # http://code.djangoproject.com/ticket/10899
#         settings.SESSION_ENGINE = 'django.contrib.sessions.backends.file'
#         engine = import_module(settings.SESSION_ENGINE)
#         store = engine.SessionStore()
#         store.save()
#         self.session = store
#         self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key
