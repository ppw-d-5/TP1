"""crowdfunding URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.urls import re_path, include
from django.conf.urls import url, include
import app_kayza.urls as donasi_uang
from app_aulia.views import *
from django.contrib.auth import logout
from django.contrib.auth import views
from app_ayu.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('donasi', include('app_kayza.urls')),
    path('history', include('profile_history.urls')),
    path('app_aulia/', include('app_aulia.urls')),
    path('', include('app_rafika.urls')),
    path('app_ayu', include('app_ayu.urls')),
    path('auth/' , include('social_django.urls', namespace='social')),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logingoogle/', logingoogle, name="logingoogle"),
    path('logout/', logoutView, name ="logout"),
    path('jsonComment/', jsonComment, name ="jsonComment"),
    path('allComment/', allComment, name ="allComment"),
]
