from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User
from django.urls import resolve
from .views import *
from .models import *
from .forms import *

class AulTest(TestCase):

	# ngecek url ada apa engga 
	def test_tp2_url_is_exist(self):
		response = Client().get('/logingoogle/')
		self.assertEqual(response.status_code, 200)

    #check text
	def test_tp1_has_welcome_text(self):
		response = Client().get('/logingoogle/')
		text = response.content.decode('utf8')
		self.assertIn("Login With Google", text)

	# def test_tp1_use_donasi(self):
	# 	found = resolve('/app_aulia/daftar/')
	# 	self.assertEqual(found.func, signup)

	# def test_model_can_create_new_todo(self):
	# 	new_activity=Signup.objects.create(nama = "Aulia", tanggal_lahir = "1999-01-03", email = "auliaramadhani34@gmail.com", password = "aaaaaa")
	# 	counting_all_available_todo=Signup.objects.all().count()
	# 	self.assertEqual(counting_all_available_todo, 1)

	# kalo blank formnya
	# def test_form_validation_for_blank_items(self):
	# 	form = Form_Signup(data={'nama': '', 'tanggal_lahir': '', 'email': '', 'password': ''})
	# 	self.assertFalse(form.is_valid())

	# test apakah sukses buat ngerender
	# def test_tp1_post_success_and_render_the_result(self):
	# 	test = 'Daftar'
	# 	response_post = Client().post('/app_aulia/daftar/', {'nama': 'Aulia', 'tanggal_lahir': '03-01-1999', 'email': 'auliaramadhani34@gmail.com', 'password': 'aaaaaaa'})
	# 	self.assertEqual(response_post.status_code, 200)

	# 	response = Client().get('/app_aulia/daftar/')
	# 	html_response = response.content.decode('utf8')
	# 	self.assertIn(test, html_response)

	# def test_form_todo_input_has_placeholder_and_css_classes(self):
	# 	form = Form_Signup()
	# 	self.assertIn('class="form-control', form.as_p())

	def test_login_app_aulia(self):
		c = Client()
		c.force_login(User.objects.get_or_create(username='testuser')[0])
		home = c.get('/logingoogle/')
		self.assertTrue(home.context['user'].is_authenticated)
		session = dict(c.session)
		self.assertTrue("username" in session)
		self.assertTrue("email" in session)
		self.assertTrue("name" in session)
		response = c.get('/logingoogle/')
		self.assertIn("Donasi", response.content.decode('utf8'))

	def test_logout_app_aulia(self):
		c = Client()
		c.force_login(User.objects.get_or_create(username='testuser')[0])
		c.logout()
		home = c.get('/logingoogle/')
		self.assertFalse(home.context['user'].is_authenticated)
		session = dict(c.session)
		self.assertFalse("username" in session)
		self.assertFalse("email" in session)
		self.assertFalse("name" in session)
		response = c.get('/logingoogle/')
		self.assertIn("Login With Google", response.content.decode('utf8'))



	# def test_nama_profil (self):
	# 	response = Client().get('/app_aulia/daftar/')
 #        profil_page = 'Registrasi'
 #        html_response = response.content.decode('utf8')
 #        self.assertIn(profil_page, html_response)

	# def test_lab6_using_template(self):
 #        response = Client().get('/app_aulia/daftar/')
 #        self.assertTemplateUsed(response, 'daftar.html')
# 	def test_tp1_post_error_and_render_the_result(self):
# 		test = 'Registrasi Donasi'
# 		response = Client().post('/app_aulia/daftar',{})
# 		self.assertEqual(response.status_code, 200)

# 		response = Client().get('/app_aulia/daftar')
# 		html_response = response.content.decode('utf8')
# 		self.assertNotIn('Rafika', html_response)