from django.apps import AppConfig


class AppAuliaConfig(AppConfig):
    name = 'app_aulia'
