from django import forms

class Form_Signup(forms.Form):
	nama = forms.CharField(label = 'Nama', required=True, max_length=100 , widget = forms.TextInput(attrs = {'size' : 100 , 'class' : 'form-control'}))
	tanggal_lahir = forms.DateField(label='Tanggal',required = True, widget = forms.DateInput(attrs = {'type' : 'date', 'class' : 'form-control'}))
	email = forms.EmailField(required=True, widget=forms.TextInput(attrs = {'class' : 'form-control'}))
    # username = forms.EmailField(widget=forms.TextInput(attrs=attrs))
	password = forms.CharField(widget=forms.TextInput(attrs = {'type' : 'password', 'class' : 'form-control'}))